import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { LoginScreen } from "./LoginScreen";
import { NavigationContainer } from "@react-navigation/native";
import { AboutScreen } from "./AboutScreen";
import { SkillScreen } from "./SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";

const Root = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const DrawerScreen = () => {
  return (
    <Drawer.Navigator initialRouteName="Tab">
      <Drawer.Screen name="AboutScreen" component={AboutScreen} />
      <Drawer.Screen name="Tab" component={TabScreen} />
    </Drawer.Navigator>
  );
};

const TabScreen = () => {
  return (
    <Tab.Navigator initialRouteName="SkillScreen">
      <Tab.Screen name="SkillScreen" component={SkillScreen} />
      <Tab.Screen name="ProjectScreen" component={ProjectScreen} />
      <Tab.Screen name="AddScreen" component={AddScreen} />
    </Tab.Navigator>
  );
};

function index() {
  return (
    <NavigationContainer>
      <Root.Navigator>
        <Root.Screen name="LoginScreen" component={LoginScreen} />
        <Root.Screen name="Drawer" component={DrawerScreen} />
      </Root.Navigator>
    </NavigationContainer>
  );
}

export default index;
